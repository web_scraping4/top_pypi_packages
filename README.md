# Top PyPi Packages
Script that opens in tabs the results of the searched keyword(s).
# Setup (linux)
- Inside the root directory (`top_pypi_packages`), install `requirements.txt`:
    ```shell
    python -m venv venv && \
    source venv/bin/activate && \
    pip install -r requirements.txt
    ```
- Run the program
    ```shell
    python main.py
    ```
