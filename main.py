#!/usr/bin/env python3
import webbrowser
import sys
import requests
from requests.exceptions import HTTPError
import bs4

BASE_URL = 'https://pypi.org'
QUERY_URL = 'https://pypi.org/search/?q='


def request_query(keywords: list) -> requests.Response:
    """
    Make a GET request to the PyPI website with a query constructed from the passed keywords.
    Raise an exception if the response status code is not 200.
    Return the response object if the request is successful.

    :param keywords:list: list of keywords to use in the query
    :return: response object
    :rtype: requests.Response
    """
    r = requests.get(QUERY_URL + ''.join(keywords))
    r.raise_for_status()
    return r


def parse_response(r: requests.Response) -> list:
    """
    Parse the text of a response object using the BeautifulSoup library.
    Select all elements with the class 'package-snippet' and the tag 'a'
    Return the selected elements as a list of HTML elements

    :param r: response object
    :type r: requests.Response
    :return: list of selected HTML elements (tag objects)
    :rtype: list
    """
    # Parse the response text using the BeautifulSoup library
    soup = bs4.BeautifulSoup(r.text, 'html.parser')
    html_elements = soup.select('a.package-snippet')
    return html_elements


def get_links(html_tags: list, limit=5) -> list:
    """
    Extracts the link to each of the elements in the html_elements list.
    Returns the first 'limit' number of links as a list.

    :param html_tags:list: list of html tags
    :param limit:int: number of links to return, defaults to 5
    :return: list of links
    :rtype: list
    """
    # Retrieve search result links
    package_links = []
    for t in html_tags:
        package_links.append(BASE_URL + t.get('href'))

    return package_links[:limit]


def open_browser_tabs(links: list) -> None:
    """
    This function allows you to open multiple browser tabs by passing a list of links.

    :param links:list: a list of links to open in the browser.
    :return: None
    """
    for l in links:
        webbrowser.open(l)


def run_program(keywords: list) -> None:
    print('searching...')
    response = request_query(keywords)
    html_tags = parse_response(response)
    links = get_links(html_tags)

    print('Opening browser tabs')
    open_browser_tabs(links)


if __name__ == '__main__':
    try:
        # Check for the correct number of command line arguments
        if len(sys.argv) < 2:
            raise ValueError(f"""
                Please provide at least 1 keyword (package name) as command line arguments.

                Example: python3 {sys.argv[0]} <keyword> [keywords]
                """)

        # Get the keywords from the command line
        keywords = sys.argv[1:]

        # Run the program
        run_program(keywords)
    except ValueError as e:
        print(f'Error: {e}')
    except HTTPError as e:
        print(f'There was a problem: {e}')
